
package eu.europa.ec.dgt.mtatec;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the eu.europa.ec.dgt.mtatec package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Asktranslation_QNAME = new QName("http://mtatec.dgt.ec.europa.eu/", "asktranslation");
    private final static QName _AsktranslationResponse_QNAME = new QName("http://mtatec.dgt.ec.europa.eu/", "asktranslationResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: eu.europa.ec.dgt.mtatec
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Asktranslation }
     * 
     */
    public Asktranslation createAsktranslation() {
        return new Asktranslation();
    }

    /**
     * Create an instance of {@link AsktranslationResponse }
     * 
     */
    public AsktranslationResponse createAsktranslationResponse() {
        return new AsktranslationResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Asktranslation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mtatec.dgt.ec.europa.eu/", name = "asktranslation")
    public JAXBElement<Asktranslation> createAsktranslation(Asktranslation value) {
        return new JAXBElement<Asktranslation>(_Asktranslation_QNAME, Asktranslation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AsktranslationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mtatec.dgt.ec.europa.eu/", name = "asktranslationResponse")
    public JAXBElement<AsktranslationResponse> createAsktranslationResponse(AsktranslationResponse value) {
        return new JAXBElement<AsktranslationResponse>(_AsktranslationResponse_QNAME, AsktranslationResponse.class, null, value);
    }

}
