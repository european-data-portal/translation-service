package de.fhg.fokus.edp.translation.batch;

import akka.actor.*;
import akka.japi.pf.DeciderBuilder;
import akka.japi.pf.ReceiveBuilder;
import de.fhg.fokus.edp.translation.model.TranslationRequest;
import kamon.Kamon;
import kamon.metric.instrument.Counter;
import scala.concurrent.duration.Duration;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by aos on 10/5/15.
 */
public class TranslationRequestRouter extends AbstractActor {
    private static SupervisorStrategy strategy =
            new OneForOneStrategy(0, Duration.Inf(), DeciderBuilder.matchAny(o -> {
                return SupervisorStrategy.stop();
            }).build());
    private Map<String, ActorRef> batchers = new HashMap<>();

    private Counter batcherStartedCounter = Kamon.metrics().counter("batcher-router-started");
    private Counter batcherStoppedCounter = Kamon.metrics().counter("batcher-router-stopped");

    TranslationRequestRouter() {
        receive(ReceiveBuilder
                .match(TranslationRequest.class, (req) -> inboundRequest(req))
                .match(Terminated.class, (t) -> batcherStopped(t.actor()))
                .build());
    }

    public static Props props() {
        return Props.create(TranslationRequestRouter.class, () -> new TranslationRequestRouter());
    }

    @Override
    public SupervisorStrategy supervisorStrategy() {
        return strategy;
    }

    public void inboundRequest(TranslationRequest req) {
        if (batchers.containsKey(req.getBatchId())) {
            batchers.get(req.getBatchId()).tell(req, self());
        } else
            createNewBatcher(req.getBatchId()).forward(req, context());
    }

    private ActorRef createNewBatcher(String batchId) {
        ActorRef ref = context().actorOf(TranslationRequestBatcher.props(batchId));
        context().watch(ref);
        batchers.put(batchId, ref);
        batcherStartedCounter.increment();
        return ref;
    }

    private void batcherStopped(ActorRef ref) {
        if (!batchers.containsValue(ref))
            throw new IllegalArgumentException("Received batcher stopped for actor not in HashMap!");

        String key = null;
        for (Map.Entry<String, ActorRef> e : batchers.entrySet()) {
            if (e.getValue().equals(ref)) {
                key = e.getKey();
                break;
            }
        }

        batcherStoppedCounter.increment();
        batchers.remove(key);
    }


}
