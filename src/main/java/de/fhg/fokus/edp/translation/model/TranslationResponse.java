package de.fhg.fokus.edp.translation.model;

/**
 * Created by aos on 9/18/15.
 */
public class TranslationResponse {
    public int code;
    public TranslationResponseBody result = null;

    public TranslationResponse(int code) {
        this.code = code;
    }

    public TranslationResponse(TranslationResponseBody result) {
        this.code = 0;
        this.result = result;
    }

    public TranslationResponse(int code, TranslationResponseBody result) {
        this.code = code;
        this.result = result;
    }
}
