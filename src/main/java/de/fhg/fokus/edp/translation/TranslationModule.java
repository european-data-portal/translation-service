package de.fhg.fokus.edp.translation;

import com.google.inject.Binder;
import com.google.inject.Module;

/**
 * Created by aos on 5/7/15.
 */
public class TranslationModule implements Module {
    @Override
    public void configure(Binder binder) {
        binder.bind(Akka.class).asEagerSingleton();
        binder.bind(ServiceResource.class);
    }
}
