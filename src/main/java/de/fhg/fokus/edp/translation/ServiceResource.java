package de.fhg.fokus.edp.translation;

import akka.actor.ActorSelection;
import akka.dispatch.Mapper;
import com.google.inject.Inject;
import de.fhg.fokus.edp.translation.message.GetTranslationDocument;
import de.fhg.fokus.edp.translation.message.TranslationError;
import de.fhg.fokus.edp.translation.message.TranslationResult;
import de.fhg.fokus.edp.translation.model.TranslationRequest;
import kamon.Kamon;
import kamon.metric.instrument.Counter;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.jboss.resteasy.annotations.Suspend;
import org.jboss.resteasy.logging.Logger;
import org.jboss.resteasy.spi.AsynchronousResponse;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;

import static akka.pattern.Patterns.ask;

/**
 * Created by aos on 5/7/15.
 */
@Path("/")
public class ServiceResource {
    private final static Logger log = Logger.getLogger(ServiceResource.class);
    @Inject
    protected Akka akka;
    private Counter requestCounter = Kamon.metrics().counter("rest-translation-request");
    private Counter errorCounter = Kamon.metrics().counter("rest-error");
    private Counter resultCounter = Kamon.metrics().counter("rest-result");

    @POST
    @Consumes("application/json")
    public void initiateTranslation(TranslationRequest translationRequest) {
        if (translationRequest == null) return;

        //log.debug("Requesting translation: " + translationRequest);
        requestCounter.increment();

        akka.batcher.tell(translationRequest, null);
    }

    @GET
    @Path("/")
    public String getRoot() {
        return "pong.";
    }

    @GET
    @Path("/processing-ids")
    @Produces("application/json")
    public void getProcessingIds(final @Suspend(5000) AsynchronousResponse response) throws Exception {
        ask(akka.service, "get-processing-ids", 5000).map(new Mapper<Object, String>() {
            @Override
            public String apply(Object oids) {
                response.setResponse(Response.ok((List<String>) oids).build());
                return "";
            }
        }, akka.system.dispatcher());
    }

    @POST
    @Path("/result")
    public void resultCallback(@Context HttpServletRequest request, @QueryParam("worker") String workerPath, @QueryParam("id") String id) throws IOException {
        if (workerPath == null || id == null) return;
        String input = IOUtils.toString(request.getInputStream());
        String path = new String(Base64.decodeBase64(workerPath));

        akka.system.actorSelection(path).tell(new TranslationResult(id, "", ""), null);
    }

    @GET
    @Path("/result")
    public void resultCallbackGet(@Context HttpServletRequest request, @QueryParam("worker") String workerPath, @QueryParam("id") String id) throws IOException {
        if (workerPath == null || id == null) return;
        String path = new String(Base64.decodeBase64(workerPath));

        resultCounter.increment();
        akka.system.actorSelection(path).tell(new TranslationResult(id), null);
    }

    @POST
    @Path("/error")
    public void errorCallback(@Context HttpServletRequest request, @QueryParam("worker") String workerPath, @QueryParam("id") String id,
                              @FormParam("errorCode") int errorCode, @FormParam("errorMessage") String errorMessage, @FormParam("targetLanguage") String targetLanguage) throws IOException {
        if (workerPath == null || id == null) return;
        String path = new String(Base64.decodeBase64(workerPath));
        errorCounter.increment();
        akka.system.actorSelection(path).tell(new TranslationError(id, errorCode, errorMessage, targetLanguage), null);
    }

    @GET
    @Path("/document")
    @Produces("application/xliff+xml")
    public void getDocument(final @Suspend(10000) AsynchronousResponse response, @QueryParam("worker") String workerPath, @QueryParam("id") String id) throws Exception {
        if (workerPath == null || id == null) {
            response.setResponse(Response.serverError().build());
            return;
        }

        String path = new String(Base64.decodeBase64(workerPath));
        ActorSelection worker = akka.system.actorSelection(path);

        ask(worker, new GetTranslationDocument(id), 9500).map(new Mapper<Object, String>() {
            @Override
            public String apply(Object parameter) {
                response.setResponse(Response.ok(parameter.toString()).build());
                return "Booyah.";
            }
        }, akka.system.dispatcher());
    }
}
