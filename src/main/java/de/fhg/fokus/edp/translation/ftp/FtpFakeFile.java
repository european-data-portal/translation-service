package de.fhg.fokus.edp.translation.ftp;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by aos on 5/15/15.
 */
public class FtpFakeFile implements org.apache.ftpserver.ftplet.FtpFile {
    private boolean exists = true, isDirectory = false, isFile = true;
    private String fileName;
    private final FtpFileSystemView fileSystemView;

    public FtpFakeFile(FtpFileSystemView fileSystemView, String fileName) {
        this.fileSystemView = fileSystemView;
        this.fileName = fileName;
    }

    @Override
    public String getAbsolutePath() {
        return "/" + fileName;
    }

    @Override
    public String getName() {
        return fileName;
    }

    @Override
    public boolean isHidden() {
        return false;
    }

    @Override
    public boolean isDirectory() {
        return isDirectory;
    }

    @Override
    public boolean isFile() {
        return isFile;
    }

    @Override
    public boolean doesExist() {
        return exists;
    }

    @Override
    public boolean isReadable() {
        return true;
    }

    @Override
    public boolean isWritable() {
        return true;
    }

    @Override
    public boolean isRemovable() {
        return true;
    }

    @Override
    public String getOwnerName() {
        return "god";
    }

    @Override
    public String getGroupName() {
        return "ofrocknroll";
    }

    @Override
    public int getLinkCount() {
        return 0;
    }

    @Override
    public long getLastModified() {
        return System.currentTimeMillis();
    }

    @Override
    public boolean setLastModified(long l) {
        return true;
    }

    @Override
    public long getSize() {
        return 0;
    }

    @Override
    public boolean mkdir() {
        this.isDirectory = true;
        this.isFile = false;
        return true;
    }

    @Override
    public boolean delete() {
        return true;
    }

    @Override
    public boolean move(org.apache.ftpserver.ftplet.FtpFile ftpFile) {
        return false;
    }

    @Override
    public List<org.apache.ftpserver.ftplet.FtpFile> listFiles() {
        return new ArrayList<>();
    }

    @Override
    public OutputStream createOutputStream(long l) throws IOException {
        return new FtpOutputStream(fileName, fileSystemView.getUser().getActorRef());
    }

    @Override
    public InputStream createInputStream(long l) throws IOException {
        return new ByteArrayInputStream(fileSystemView.getDocument().getBody().getBytes());
    }
}
