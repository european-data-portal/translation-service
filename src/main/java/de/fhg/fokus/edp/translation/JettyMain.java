package de.fhg.fokus.edp.translation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.ObjectMapper;
import com.mashape.unirest.http.Unirest;
import kamon.Kamon;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.WebAppContext;

import java.io.IOException;

/**
 * Created by aos on 10/5/15.
 */
public class JettyMain {

    private static void configureUnirest() {
        Unirest.setObjectMapper(new ObjectMapper() {
            private com.fasterxml.jackson.databind.ObjectMapper jacksonObjectMapper
                    = new com.fasterxml.jackson.databind.ObjectMapper();

            public <T> T readValue(String value, Class<T> valueType) {
                try {
                    return jacksonObjectMapper.readValue(value, valueType);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }

            public String writeValue(Object value) {
                try {
                    return jacksonObjectMapper.writeValueAsString(value);
                } catch (JsonProcessingException e) {
                    throw new RuntimeException(e);
                }
            }
        });


        // XXX - calculate these values from worker settings
        Unirest.setConcurrency(100, 2);
    }

    public static void main(String[] args) throws Exception {
        Kamon.start();
        final int port = 8080;

        configureUnirest();

        Server server = new Server(port);

        WebAppContext webapp = new WebAppContext();
        webapp.setDefaultsDescriptor("WEB-INF/web.xml");
        webapp.setContextPath("/translation-service");
        webapp.setWar(".");
        webapp.setParentLoaderPriority(true);
        webapp.setServer(server);
        server.setHandler(webapp);

        server.start();
        server.join();

        Kamon.shutdown();
    }
}
