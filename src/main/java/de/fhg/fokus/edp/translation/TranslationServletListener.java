package de.fhg.fokus.edp.translation;

import com.google.inject.Inject;
import kamon.Kamon;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Created by aos on 10/5/15.
 */
public class TranslationServletListener implements ServletContextListener {
    @Inject
    protected Akka akka;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        Kamon.start();

    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        System.err.println("DESTROY DESTROY DESTROY SERVLET");
        Kamon.shutdown();
        akka.system.terminate();
    }
}
