package de.fhg.fokus.edp.translation.ftp;

import akka.actor.ActorRef;
import org.apache.ftpserver.ftplet.FileSystemFactory;
import org.apache.ftpserver.ftplet.FileSystemView;
import org.apache.ftpserver.ftplet.FtpException;
import org.apache.ftpserver.ftplet.User;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by aos on 5/15/15.
 */
public class FtpFileSystemFactory implements FileSystemFactory {
    private HashMap<FtpUser, FtpFileSystemView> views = new HashMap<>();

    @Override
    public FileSystemView createFileSystemView(User user) throws FtpException {
        return views.get(user);
    }

    public void delete(ActorRef actor) {
        if (views.containsKey(actor))
            views.remove(actor);

        // FIXME how to treat failure?
    }

    public FtpFileSystemView create(FtpUser user) {
        FtpFileSystemView view = new FtpFileSystemView(user);
        views.put(user, view);
        return view;
    }

    public FtpFileSystemView getFileSystemByActorRef(ActorRef ref) {
        for (Map.Entry<FtpUser, FtpFileSystemView> entry : views.entrySet()) {
            if (entry.getKey().getActorRef().equals(ref)) return entry.getValue();
        }

        return null;
    }
}
