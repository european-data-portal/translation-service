akka {
  # Loggers to register at boot time (akka.event.Logging$DefaultLogger logs
  # to STDOUT)
  loggers = ["akka.event.slf4j.Slf4jLogger"]
  loglevel = "DEBUG"
  logging-filter = "akka.event.slf4j.Slf4jLoggingFilter"

  persistence.journal.plugin = "akka.persistence.journal.leveldb"
  persistence.journal.leveldb.dir = "./persistence/leveldb"
  persistence.snapshot-store.local.dir = "./persistence/snapshots"
  persistence.snapshot-store.plugin = "akka.persistence.snapshot-store.local"
}

kamon {

  metric {
    tick-interval = 30s
  }

  statsd {
    hostname = "localhost"
    port = 8125

    flush-interval = 30s

    subscriptions {
      histogram = ["**"]
      min-max-counter = ["**"]
      gauge = ["**"]
      counter = ["**"]
      trace = ["**"]
      trace-segment = ["**"]
      akka-actor = ["**"]
      akka-dispatcher = ["**"]
      akka-router = ["**"]
      system-metric = ["**"]
      http-server = []
    }

    simple-metric-key-generator {
      include-hostname = true
      application = "translation"
    }
  }
}

translation {
  batch-size = 300
  batch-timeout = 5m

  worker-count = 10
  url = "http://www.europeandataportal.eu/translation-service"
  endpoint = "https://webgate.ec.europa.eu/MtatecOsbConnector/MtatecOsbConnector/InboundConnectorSimpleProxyService"

  snapshot-interval = 24h
  translation-timeout = 2h

  mock = false

  wsdl {
    username = ""
    application-name = ""
  }

  ftp {
    listen {
      host = "0.0.0.0"
      port = 49500
      passive-ports = "49501-49506"
    }
    external-address = "www.europeandataportal.eu"
  }

  mail {
    server = "mx"
    ssl = false
    port = 25
    auth = false
    username = ""
    password = ""
    sender = "noreply@europeandataportal.eu"
    xlifrec = ["leojulian.li@fokus.fraunhofer.de"]
    recipients = ["fabian.kirstein@fokus.fraunhofer.de"]
    debug = false

    skip = false
  }
}

include "env"
include "credentials"